import {Request, Response} from 'express';

export type HttpExchange = {
  request: Request,
  response: Response,
};
