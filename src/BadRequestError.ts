import {HttpError} from './HttpError';

export class BadRequestError extends HttpError {
  constructor(message: string, title: string = 'Bad Request', customCode: string = 'ERR-400', httpCode: number = 400) {
    super(message, title, customCode, httpCode);
  }
}
