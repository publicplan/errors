export class BaseError extends Error {
  private __proto__: Object;

  constructor(...args) {
    super(...args);
    Error.captureStackTrace(this, BaseError);

    /**
     * Allow extending built-ins to fix the prototype chain. See:
     * https://github.com/Microsoft/TypeScript/wiki/Breaking-Changes#extending-built-ins-like-error-array-and-map-may-no-longer-work
     * https://stackoverflow.com/a/48342359/199072
     */
    const actualPrototype = new.target.prototype;
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(this, actualPrototype);
    } else {
      this.__proto__ = actualPrototype;
    }
  }
}
