import {HttpError} from "./HttpError";

export class InternalServerError extends HttpError {
  constructor(
    message: string,
    title: string = 'Internal Server Error',
    customCode: string = 'ERR-500',
    httpCode: number = 500,
  ) {
    super(message, title, customCode, httpCode);
  }
}
