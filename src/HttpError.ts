import {BaseError} from './BaseError';

export abstract class HttpError extends BaseError {
  protected customCode: string;
  protected title: string;
  public httpCode: number;

  protected constructor(message: string, title: string, customCode: string, httpCode: number) {
    super(message);
    this.message = message;
    this.title = title;
    this.customCode = customCode;
    this.httpCode = httpCode;
  }

  public toJson() {
    return {
      code: this.customCode,
      titel: this.title,
      nachricht: this.message,
    };
  }
}
