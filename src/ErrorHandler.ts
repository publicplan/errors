import xmlBuilder from 'xmlbuilder';
import {HttpExchange} from './types';
import {InternalServerError} from "./InternalServerError";

export class ErrorHandler {
  private error: any;
  private httpExchange: HttpExchange;

  constructor(error: any, httpExchange: HttpExchange) {
    this.error = error;
    this.httpExchange = httpExchange;
  }

  public handle() {
    this.convertGenericErrorToInternalServerError();
    this.httpExchange.response.status(this.error.httpCode);
    this.httpExchange.response.format({
      'application/json': () => this.httpExchange.response.json({fehler: [this.error.toJson()]}),
      'application/xml': () => {
        const xml = xmlBuilder.create(
          {fehler: {Fehler: [this.error.toJson()]}},
          {version: '1.0', encoding: 'utf8'},
        );
        return this.httpExchange.response.set('Content-Type', 'application/xml').send(xml.end());
      },
    });
  }

  private convertGenericErrorToInternalServerError() {
    if (!this.error.hasOwnProperty('httpCode')) {
      this.error = new InternalServerError(
        'Es ist ein unbekannter Fehler aufgetreten. Bitte versuchen Sie es nochmal.',
        'Unbekannter Fehler',
      );
    }
  }
}
