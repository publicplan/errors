
# Errors

Eine Bibliothek, um Http Fehler abzubilden und mit einer Express Middleware zu behandeln. 

## Anleitung

Definiere eine Express Middleware nach allen Route Händlern, um die Fehler zu behandeln. Rufe die Funktion 
`NextFunction` mit dem entsprechenden Fehler-Objekt in den Route Händlern bei Bedarf.
```typescript
  import {ErrorHandler, BadRequestError} from 'errors';
  import express, {Application, NextFunction, Request, Response, Router} from 'express';
  
  const router: Router = Router();
  function wowRoute() {
    return router.get('/', (req: Request, res: Response, next: NextFunction) => {
      next(new BadRequestError('Fehler Nachricht.', 'Fehlertitel'));
    });
  }
  
  function errorHandler() {
    return router.use((error: any, request: Request, response: Response, next: NextFunction) => {
      new ErrorHandler(error, {request, response}).handle();
    });
  }
  
  const application: Application = express();
  application.use(wowRoute());
  application.use(errorHandler());
```

## Installation
```bash
npm install --save "git+https://git@bitbucket.org/publicplan/errors.git"
```
